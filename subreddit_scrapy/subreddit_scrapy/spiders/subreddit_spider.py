import scrapy

from subreddit_scrapy.items import SubredditScrapyItem


class SubredditSpider(scrapy.Spider):
    name = "subreddit"
    start_urls = [
        'https://www.reddit.com/r/Python/'
    ]

    def parse(self, response):
        submissions = response.xpath(
            '//div[@id="siteTable"]/div[contains(@class, "thing")]')
        for post in submissions:
            item = SubredditScrapyItem()
            item['id'] = post.xpath('.//@id').extract_first()
            item['title'] = post.xpath(
                './/p[@class="title"]/a/text()').extract_first()
            item['submitter'] = post.xpath(
                './/@data-author').extract_first()
            item['external_url'] = post.xpath(
                './/p[@class="title"]/a/@data-outbound-url').extract_first()
            item['discussion_url'] = post.xpath(
                './/li[@class="first"]/a/@href').extract_first()
            item['punctuation'] = post.xpath(
                './/@data-score').extract_first()
            item['creation_date'] = post.xpath(
                './/p[contains(@class,"tagline")]/time/@datetime').extract_first()
            item['num_comments'] = post.xpath(
                './/@data-comments-count').extract_first()

            yield item

        next_page = response.xpath(
            '//span[@class="next-button"]/a/@href').extract_first()

        # If next button exists, continue scrapying next page
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)
