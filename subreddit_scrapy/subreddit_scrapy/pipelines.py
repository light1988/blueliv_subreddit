# -*- coding: utf-8 -*-

import logging
from datetime import datetime

from scrapy.exceptions import DropItem

from blueliv_rest.models import SubredditUser, Submission


class SubredditScrapyPipeline(object):

    def __init__(self):
        self.logger = logging.getLogger()

    def process_item(self, item, spider):
        if self.is_valid(item):
            user, submission = self.convert_to_model(item)
            if user and submission:
                try:
                    # Create new subreddit user or update if exists
                    subreddit_user, created = SubredditUser.objects.update_or_create(
                        user_name=user.user_name,
                    )
                    # Create new subreddit submission or update if exists
                    Submission.objects.update_or_create(
                        subreddit_id=submission.subreddit_id,
                        defaults={
                            'title': submission.title,
                            'external_url': submission.external_url,
                            'submitter': subreddit_user,
                            'discussion_url': submission.discussion_url,
                            'punctuation': submission.punctuation,
                            'creation_date': submission.creation_date,
                            'num_comments': submission.num_comments,
                        }

                    )
                except Exception as e:
                    self.logger.error("Could not persist to database")
                    self.logger.error(str(e))
            else:
                raise DropItem(
                    "Could not convert item {} to model".format(item)
                )
        else:
            raise DropItem("Missing fields in {}".format(item))
        return item

    def is_valid(self, item):
        """ Method for validating scrapped data """

        to_ret = True

        if (
            not item.get('id') or not item.get('title') or
            not item.get('submitter') or not item.get('discussion_url') or
            item.get('punctuation') is None or
            not item.get('punctuation').isdigit() or
            item.get('creation_date') is None or
            item.get('num_comments') is None or
            not item.get('num_comments').isdigit()
        ):
            to_ret = False

        return to_ret

    def convert_to_model(self, item):
        """ Method for converting scrapy items to django models """

        user = None
        submission = None
        try:
            user = SubredditUser(user_name=item.get('submitter'))

            title = (
                str(item.get('title'))[:300]
                if len(str(item.get('title'))) > 300
                else str(item.get('title'))
            )

            submission = Submission(
                subreddit_id=item.get('id'),
                title=title,
                external_url=item.get('external_url'),
                discussion_url=item.get('discussion_url'),
                punctuation=int(item.get('punctuation')),
                creation_date=datetime.strptime(
                    ''.join(
                        item.get('creation_date').rsplit(':', 1)
                    ), "%Y-%m-%dT%H:%M:%S%z"
                ),
                num_comments=int(item.get('num_comments'))
            )
        except Exception as e:
            self.logger.error("Invalid item")
            self.logger.error(str(e))
        finally:
            return user, submission
