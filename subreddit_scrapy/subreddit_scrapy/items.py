# -*- coding: utf-8 -*-


import scrapy


class SubredditScrapyItem(scrapy.Item):
    """ Class for saving subreddit scrapy items """

    id = scrapy.Field()
    title = scrapy.Field()
    submitter = scrapy.Field()
    external_url = scrapy.Field()
    discussion_url = scrapy.Field()
    punctuation = scrapy.Field()
    creation_date = scrapy.Field()
    num_comments = scrapy.Field()
