# Local project configuration (python3, python3_dev, virtualenv and curl must have been installed). Tested with Python 3.5.2 and Ubuntu 16.04.

## Configuration

### Set up virtual environment

```
mkdir blueliv
virtualenv -p python3 blueliv/
cd blueliv/
source bin/activate
```

### Clone repository


`git clone git@gitlab.com:light1988/blueliv_subreddit.git`


### Install libraries

```
cd blueliv_subreddit/
pip install -r requirements.txt
```

### Database coinfiguration

```
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
```
```
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
```
```
sudo apt-get update
sudo apt-get install postgresql-9.6

sudo su - postgres
psql
create user blueliv with password 'blueliv';
create database blueliv;
ALTER ROLE blueliv SET client_encoding TO 'utf8';
ALTER ROLE blueliv SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE blueliv TO blueliv;
\q
exit

cd blueliv_api/
python manage.py migrate
```

## Execute spider

`cd ../subreddit_scrapy/`

* First execution

`scrapy crawl subreddit`

* Second Execution (for adding new records and update old ones)

`scrapy crawl subreddit`


## Start server

```
cd ../blueliv_api/
python manage.py runserver
```

## Testing resources

* Open new console

### Resource for getting top 10 submissions by points

#### All types

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/submissions/popular
`

* Response:

```
HTTP/1.0 200 OK
Date: Sat, 06 Jan 2018 16:04:52 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Allow: GET, HEAD, OPTIONS
Content-Length: 1347
Content-Type: application/json
X-Frame-Options: SAMEORIGIN
Vary: Accept, Cookie

[{"id":469,"submitter_name":"backprop88","title":"Automate the boring stuff with python - tinder","punctuation":6144,"num_comments":359},{"id":587,"submitter_name":"Chilangosta","title":"MS is considering official Python integration with Excel, and is asking for input","punctuation":4382,"num_comments":423},{"id":199,"submitter_name":"winner_godson","title":"Python Cheet Sheet for begineers","punctuation":3812,"num_comments":133},{"id":534,"submitter_name":"amachefe","title":"Microsoft Considers Adding Python As an Official Scripting Language in Excel","punctuation":2578,"num_comments":172},{"id":3,"submitter_name":"fabrikated","title":"Really??","punctuation":1721,"num_comments":299},{"id":20,"submitter_name":"lectorlector","title":"Hacking WiFi to inject cryptocurrency miner to HTML requests with Python","punctuation":869,"num_comments":76},{"id":232,"submitter_name":"harrybosgrandad","title":"Turtle module drawing a randomised landscape","punctuation":780,"num_comments":45},{"id":131,"submitter_name":"harrybosgrandad","title":"User controlled Turtle cubes","punctuation":725,"num_comments":15},{"id":186,"submitter_name":"Retzudo","title":"A REAL Python cheat sheet for beginners","punctuation":711,"num_comments":23},{"id":949,"submitter_name":"LewisTheScot","title":"Django 2.0 Released","punctuation":657,"num_comments":172}]
```

#### Just discussion

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/submissions/popular?type=discussion
`

* Response:

```
HTTP/1.0 200 OK
Date: Sat, 06 Jan 2018 16:13:26 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Type: application/json
Allow: GET, HEAD, OPTIONS
X-Frame-Options: SAMEORIGIN
Vary: Accept, Cookie
Content-Length: 1575

[{"id":199,"submitter_name":"winner_godson","title":"Python Cheet Sheet for begineers","punctuation":3812,"num_comments":133},{"id":131,"submitter_name":"harrybosgrandad","title":"User controlled Turtle cubes","punctuation":725,"num_comments":15},{"id":171,"submitter_name":"tusharg19","title":"#PyCon 2016. Guido van Rossum the founder of Python programming language explained very beautifully why does python exists in the World today.","punctuation":301,"num_comments":29},{"id":727,"submitter_name":"Darkmere","title":"Are you an hobbyist or junior dev looking to improve?","punctuation":264,"num_comments":91},{"id":579,"submitter_name":"asamolion","title":"First ever PyCon happening in Pakistan","punctuation":223,"num_comments":36},{"id":331,"submitter_name":"codewriter404","title":"Anyone have any tricks to understanding and using classes in Python? I keep writing functions, instead. Why should I start using classes?","punctuation":194,"num_comments":67},{"id":662,"submitter_name":"HeWhoWritesCode","title":"('should I %s or should I {}?' % ('stay')).format('go')","punctuation":182,"num_comments":122},{"id":381,"submitter_name":"Troge","title":"A mini-tutorial on deploying smart contracts with python","punctuation":177,"num_comments":4},{"id":106,"submitter_name":"DryerLintJockStrap","title":"Humble bundle has 15 books on python for 12 bucks right now... Is the potential opportunity I'm looking for?","punctuation":160,"num_comments":82},{"id":670,"submitter_name":"winner_godson","title":"Free Python Udemy Courses","punctuation":151,"num_comments":31}]
```

#### External links

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/submissions/popular?type=external
`

* Response:

```
HTTP/1.0 200 OK
Date: Sat, 06 Jan 2018 16:14:50 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Type: application/json
Allow: GET, HEAD, OPTIONS
X-Frame-Options: SAMEORIGIN
Vary: Accept, Cookie
Content-Length: 1452

[{"id":469,"submitter_name":"backprop88","title":"Automate the boring stuff with python - tinder","punctuation":6144,"num_comments":359},{"id":587,"submitter_name":"Chilangosta","title":"MS is considering official Python integration with Excel, and is asking for input","punctuation":4382,"num_comments":423},{"id":534,"submitter_name":"amachefe","title":"Microsoft Considers Adding Python As an Official Scripting Language in Excel","punctuation":2578,"num_comments":172},{"id":3,"submitter_name":"fabrikated","title":"Really??","punctuation":1721,"num_comments":299},{"id":20,"submitter_name":"lectorlector","title":"Hacking WiFi to inject cryptocurrency miner to HTML requests with Python","punctuation":869,"num_comments":76},{"id":232,"submitter_name":"harrybosgrandad","title":"Turtle module drawing a randomised landscape","punctuation":780,"num_comments":45},{"id":186,"submitter_name":"Retzudo","title":"A REAL Python cheat sheet for beginners","punctuation":711,"num_comments":23},{"id":949,"submitter_name":"LewisTheScot","title":"Django 2.0 Released","punctuation":657,"num_comments":172},{"id":78,"submitter_name":"A3gis","title":"Google's official python tutorial violates PEP8, recommends python 2.4-2.6, and my university just assigned it to us.","punctuation":599,"num_comments":271},{"id":637,"submitter_name":"ageitgey","title":"How to break a CAPTCHA system in 15 minutes with Machine Learning","punctuation":581,"num_comments":56}]
```

#### Fail

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/submissions/popular?type=dasd
`

* Response:

```
HTTP/1.0 400 Bad Request
Date: Sat, 06 Jan 2018 16:16:07 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Length: 0
Allow: GET, HEAD, OPTIONS
X-Frame-Options: SAMEORIGIN
Vary: Accept, Cookie
```

### Resource for getting top 10 discussed submissions

#### All types

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/submissions/discussions
`

* Response:

```
HTTP/1.0 200 OK
Date: Sat, 06 Jan 2018 16:20:09 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Type: application/json
Allow: GET, HEAD, OPTIONS
X-Frame-Options: SAMEORIGIN
Vary: Accept, Cookie
Content-Length: 1443

[{"id":587,"submitter_name":"Chilangosta","title":"MS is considering official Python integration with Excel, and is asking for input","punctuation":4382,"num_comments":423},{"id":469,"submitter_name":"backprop88","title":"Automate the boring stuff with python - tinder","punctuation":6144,"num_comments":359},{"id":3,"submitter_name":"fabrikated","title":"Really??","punctuation":1721,"num_comments":299},{"id":78,"submitter_name":"A3gis","title":"Google's official python tutorial violates PEP8, recommends python 2.4-2.6, and my university just assigned it to us.","punctuation":599,"num_comments":271},{"id":534,"submitter_name":"amachefe","title":"Microsoft Considers Adding Python As an Official Scripting Language in Excel","punctuation":2578,"num_comments":172},{"id":949,"submitter_name":"LewisTheScot","title":"Django 2.0 Released","punctuation":657,"num_comments":172},{"id":653,"submitter_name":"diesch","title":"Ubuntu Devs Work on Demoting Python 2 to \"Universe\" Repo for Ubuntu 18.04 LTS","punctuation":404,"num_comments":153},{"id":846,"submitter_name":"cauebs","title":"PEP 557 (Data Classes) has been accepted!","punctuation":511,"num_comments":149},{"id":199,"submitter_name":"winner_godson","title":"Python Cheet Sheet for begineers","punctuation":3812,"num_comments":133},{"id":662,"submitter_name":"HeWhoWritesCode","title":"('should I %s or should I {}?' % ('stay')).format('go')","punctuation":182,"num_comments":122}]
```

#### Just discussion

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/submissions/discussions?type=discussion
`

* Response:

```
HTTP/1.0 200 OK
Date: Sat, 06 Jan 2018 16:19:22 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Type: application/json
Allow: GET, HEAD, OPTIONS
X-Frame-Options: SAMEORIGIN
Vary: Accept, Cookie
Content-Length: 1437

[{"id":199,"submitter_name":"winner_godson","title":"Python Cheet Sheet for begineers","punctuation":3812,"num_comments":133},{"id":662,"submitter_name":"HeWhoWritesCode","title":"('should I %s or should I {}?' % ('stay')).format('go')","punctuation":182,"num_comments":122},{"id":328,"submitter_name":"AutoModerator","title":"What's everyone working on this week?","punctuation":23,"num_comments":105},{"id":827,"submitter_name":"TheClickerMan","title":"Has anyone here gotten a job after teaching themselves python?","punctuation":116,"num_comments":96},{"id":727,"submitter_name":"Darkmere","title":"Are you an hobbyist or junior dev looking to improve?","punctuation":264,"num_comments":91},{"id":673,"submitter_name":"AutoModerator","title":"What's everyone working on this week?","punctuation":33,"num_comments":87},{"id":106,"submitter_name":"DryerLintJockStrap","title":"Humble bundle has 15 books on python for 12 bucks right now... Is the potential opportunity I'm looking for?","punctuation":160,"num_comments":82},{"id":532,"submitter_name":"Theriley106","title":"What coding practices separate an amateur Python programmer from a professional one?","punctuation":78,"num_comments":74},{"id":1,"submitter_name":"aphoenix","title":"/r/Python official Job Board!","punctuation":71,"num_comments":68},{"id":866,"submitter_name":"AutoModerator","title":"What's everyone working on this week?","punctuation":19,"num_comments":68}]
```

#### External links

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/submissions/discussions?type=external
`

* Response:

```
HTTP/1.0 200 OK
Date: Sat, 06 Jan 2018 16:18:32 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Type: application/json
Allow: GET, HEAD, OPTIONS
X-Frame-Options: SAMEORIGIN
Vary: Accept, Cookie
Content-Length: 1487

[{"id":587,"submitter_name":"Chilangosta","title":"MS is considering official Python integration with Excel, and is asking for input","punctuation":4382,"num_comments":423},{"id":469,"submitter_name":"backprop88","title":"Automate the boring stuff with python - tinder","punctuation":6144,"num_comments":359},{"id":3,"submitter_name":"fabrikated","title":"Really??","punctuation":1721,"num_comments":299},{"id":78,"submitter_name":"A3gis","title":"Google's official python tutorial violates PEP8, recommends python 2.4-2.6, and my university just assigned it to us.","punctuation":599,"num_comments":271},{"id":949,"submitter_name":"LewisTheScot","title":"Django 2.0 Released","punctuation":657,"num_comments":172},{"id":534,"submitter_name":"amachefe","title":"Microsoft Considers Adding Python As an Official Scripting Language in Excel","punctuation":2578,"num_comments":172},{"id":653,"submitter_name":"diesch","title":"Ubuntu Devs Work on Demoting Python 2 to \"Universe\" Repo for Ubuntu 18.04 LTS","punctuation":404,"num_comments":153},{"id":846,"submitter_name":"cauebs","title":"PEP 557 (Data Classes) has been accepted!","punctuation":511,"num_comments":149},{"id":20,"submitter_name":"lectorlector","title":"Hacking WiFi to inject cryptocurrency miner to HTML requests with Python","punctuation":869,"num_comments":76},{"id":779,"submitter_name":"omg_drd4_bbq","title":"Pandas: Meet the man behind the most important tool in data science","punctuation":352,"num_comments":66}]
```

#### Fail

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/submissions/discussions?type=23
`

* Response:

```
HTTP/1.0 400 Bad Request
Date: Sat, 06 Jan 2018 16:18:04 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Length: 0
Allow: GET, HEAD, OPTIONS
X-Frame-Options: SAMEORIGIN
Vary: Accept, Cookie
```

### Resource for getting top 10 submitters

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/users/top
`

* Response:

```
HTTP/1.0 200 OK
Date: Sat, 06 Jan 2018 17:36:13 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Type: application/json
X-Frame-Options: SAMEORIGIN
Allow: GET, HEAD, OPTIONS
Content-Length: 591
Vary: Accept, Cookie

[{"id":66,"user_name":"winner_godson","total_submissions":16},{"id":103,"user_name":"Especuloide","total_submissions":10},{"id":10,"user_name":"jfishersolutions","total_submissions":7},{"id":65,"user_name":"blarghmatey","total_submissions":7},{"id":94,"user_name":"TheGuru12","total_submissions":7},{"id":318,"user_name":"cloudyrathor","total_submissions":6},{"id":305,"user_name":"japaget","total_submissions":6},{"id":8,"user_name":"anuragrana311","total_submissions":6},{"id":217,"user_name":"breamoreboy","total_submissions":6},{"id":307,"user_name":"Moondra2017","total_submissions":5}]
```

### Resource for getting all submissions by give user

#### User found

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/users/10/submissions/
`

* Response:

```
HTTP/1.0 200 OK
Date: Sat, 06 Jan 2018 20:16:39 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Type: application/json
X-Frame-Options: SAMEORIGIN
Content-Length: 1027
Allow: GET, HEAD, OPTIONS
Vary: Accept, Cookie

[{"id":10,"submitter_name":"jfishersolutions","title":"Functions In Python","punctuation":1,"num_comments":0},{"id":72,"submitter_name":"jfishersolutions","title":"How To Create Malware With Python: Bitcoin Thief","punctuation":0,"num_comments":0},{"id":89,"submitter_name":"jfishersolutions","title":"Python Zero to Hero: What Are Variables?","punctuation":0,"num_comments":0},{"id":163,"submitter_name":"jfishersolutions","title":"How To Download Python And Write Your First Program!","punctuation":0,"num_comments":0},{"id":198,"submitter_name":"jfishersolutions","title":"Why You Should Learn Python In 2018!","punctuation":0,"num_comments":0},{"id":645,"submitter_name":"jfishersolutions","title":"Create J.A.R.V.I.S With Python | A Voice Activated Desktop Assistant Tutorial | Feedback Appreciated!","punctuation":31,"num_comments":2},{"id":813,"submitter_name":"jfishersolutions","title":"Python Web Scraping & Sentiment Analysis Tutorial For Beginners | Top 100 Subreddits Sentiment","punctuation":27,"num_comments":0}]
```

#### User not found

`
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8000/users/10000/submissions/
`

* Response:

```
HTTP/1.0 404 Not Found
Date: Sat, 06 Jan 2018 18:16:52 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Content-Length: 0
Vary: Accept, Cookie
X-Frame-Options: SAMEORIGIN
Allow: GET, HEAD, OPTIONS
```

## Tests Execution

* In main console:

Ctrl + C to stop the Server

`python manage.py test`


## Decissions made

I have used 3 third party libraries. I chose django and django-rest-framework because
you can easily develop rest API and manage databases. I've use scrapy to use my spider
cause is the main python library for web scraping.

I also used pyscopg2 as python conector por postgresql. I decided to use postgresql
cause it's very powerful like Oracle but with advantage of being free.

## Suggestions for improvement

In order to improve scrapy performance, database inserts should be do as a bulk create
(insert many data at the same time). Another change could be use a NO SQL database
(e.g. MongoDB)

It would be necessary to create tests for spider.
