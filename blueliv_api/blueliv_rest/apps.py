from django.apps import AppConfig


class BluelivRestConfig(AppConfig):
    name = 'blueliv_rest'
