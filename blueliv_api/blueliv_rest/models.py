from django.db import models
from django.db.models import Count


class SubredditUser(models.Model):
    """ Class for subreddit users """

    user_name = models.CharField(max_length=50, unique=True)


class Submission(models.Model):
    """ Class for subreddit submissions """

    subreddit_id = models.CharField(max_length=50)
    title = models.CharField(max_length=300)
    submitter = models.ForeignKey('SubredditUser')
    external_url = models.TextField(null=True)
    discussion_url = models.TextField()
    punctuation = models.IntegerField(default=0)
    creation_date = models.DateTimeField()
    num_comments = models.IntegerField(default=0)
