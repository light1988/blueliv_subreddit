from django.conf.urls import url
from blueliv_rest import views


urlpatterns = [
    url(r'^submissions/popular$', views.MostPopular.as_view(), name='popular'),
    url(r'^submissions/discussions$',
        views.MostDiscussed.as_view(), name='discussions'),
    url(r'^users/top$',
        views.TopSubmitters.as_view(), name='submitters'),
    url(r'^users/(?P<user_id>\d+)/submissions/$',
        views.UserSubmissions.as_view(), name='user_posts'),
]
