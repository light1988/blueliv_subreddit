import json
from datetime import datetime

from django.test import TestCase, TransactionTestCase
from django.urls import reverse, resolve
from rest_framework import status

from blueliv_rest.models import Submission, SubredditUser


class TestMostPopular(TestCase):
    """ Test module for MostPopular get """

    def setUp(self):
        self.invalid_type_param1 = "type=ext"
        self.invalid_type_param2 = "type=234"
        self.external_type_param = "type=external"
        self.discussion_type_param = "type=discussion"

    def test_invalid_type_param(self):

        response = self.client.get(
            '/submissions/popular?{}'.format(self.invalid_type_param1)
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.get(
            '/submissions/popular?{}'.format(self.invalid_type_param2)
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_method_not_allowed(self):

        response = self.client.post(
            reverse('popular'),
            data=json.dumps({'type': ''}),
            content_type='application/json'
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_405_METHOD_NOT_ALLOWED
        )

    def test_valid_resources(self):

        response = self.client.get(
            '/submissions/popular?{}'.format(self.external_type_param)
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(
            '/submissions/popular?{}'.format(self.discussion_type_param)
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(reverse('popular'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestMostDiscussed(TestCase):
    """ Test module for MostDiscussed get """

    def setUp(self):
        self.invalid_type_param1 = "type= "
        self.invalid_type_param2 = "type=dsad"
        self.external_type_param = "type=external"
        self.discussion_type_param = "type=discussion"

    def test_invalid_type_param(self):

        response = self.client.get(
            '/submissions/discussions?{}'.format(self.invalid_type_param1)
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.get(
            '/submissions/discussions?{}'.format(self.invalid_type_param2)
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_method_not_allowed(self):

        response = self.client.post(
            reverse('discussions'),
            data=json.dumps({'type': ''}),
            content_type='application/json'
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_405_METHOD_NOT_ALLOWED
        )

    def test_valid_resources(self):

        response = self.client.get(
            '/submissions/discussions?{}'.format(self.external_type_param)
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(
            '/submissions/discussions?{}'.format(self.discussion_type_param)
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(reverse('discussions'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestTopSubmitters(TestCase):
    """ Test module for TopSubmitters get """

    def test_method_not_allowed(self):

        response = self.client.post(
            reverse('submitters'),
            content_type='application/json'
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_405_METHOD_NOT_ALLOWED
        )

    def test_valid_resources(self):

        response = self.client.get(reverse('submitters'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestUserSubmissions(TestCase):
    """ Test module for UserSubmissions get """

    def setUp(self):
        self.valid_param = 10
        self.user_not_found_param = 10000

    def test_user_not_found_param(self):

        response = self.client.get(
            '/users/{}/submissions/'.format(self.user_not_found_param)
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_method_not_allowed(self):

        response = self.client.post(
            '/users/{}/submissions/'.format(self.valid_param),
            content_type='application/json'
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_405_METHOD_NOT_ALLOWED
        )


class TestsResponsesContent(TransactionTestCase):
    """ Test module for verify responses content for each resource """
    reset_sequences = True

    def setUp(self):
        self.invalid_type_param1 = "type=ext"
        self.invalid_type_param2 = "type=234"
        self.external_type_param = "type=external"
        self.discussion_type_param = "type=discussion"

        sub_user1 = SubredditUser(user_name='user1')
        sub_user2 = SubredditUser(user_name='user2')
        sub_user3 = SubredditUser(user_name='user3')
        sub_user4 = SubredditUser(user_name='user4')
        sub_user1.save()
        sub_user2.save()
        sub_user3.save()
        sub_user4.save()

        submission1 = Submission(
            title="Título 1",
            submitter=sub_user1,
            external_url=None,
            discussion_url='https://www.reddit.com/1/test',
            punctuation=340,
            creation_date=datetime.strptime(
                "2018-01-01T12:42:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=200
        )
        submission2 = Submission(
            title="Título 2",
            submitter=sub_user2,
            external_url=None,
            discussion_url='https://www.reddit.com/2/test',
            punctuation=67,
            creation_date=datetime.strptime(
                "2018-01-02T18:45:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=500
        )
        submission3 = Submission(
            title="Título 3",
            submitter=sub_user3,
            external_url='https://www.stackoverflow.com',
            discussion_url='https://www.reddit.com/3/test',
            punctuation=650,
            creation_date=datetime.strptime(
                "2018-01-01T06:42:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=1000
        )
        submission4 = Submission(
            title="Título 4",
            submitter=sub_user4,
            external_url='https://www.google.es',
            discussion_url='https://www.reddit.com/4/test',
            punctuation=789,
            creation_date=datetime.strptime(
                "2017-02-01T10:22:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=5
        )
        submission5 = Submission(
            title="Título 5",
            submitter=sub_user1,
            external_url=None,
            discussion_url='https://www.reddit.com/5/test',
            punctuation=23,
            creation_date=datetime.strptime(
                "2018-01-03T12:42:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=128
        )
        submission6 = Submission(
            title="Título 6",
            submitter=sub_user1,
            external_url=None,
            discussion_url='https://www.reddit.com/6/test',
            punctuation=99,
            creation_date=datetime.strptime(
                "2018-01-04T18:45:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=180
        )
        submission7 = Submission(
            title="Título 7",
            submitter=sub_user1,
            external_url='https://www.stackoverflow.com',
            discussion_url='https://www.reddit.com/7/test',
            punctuation=2,
            creation_date=datetime.strptime(
                "2018-01-05T06:42:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=7
        )
        submission8 = Submission(
            title="Título 8",
            submitter=sub_user2,
            external_url=None,
            discussion_url='https://www.reddit.com/8/test',
            punctuation=524,
            creation_date=datetime.strptime(
                "2018-01-05T12:42:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=170
        )
        submission9 = Submission(
            title="Título 9",
            submitter=sub_user2,
            external_url=None,
            discussion_url='https://www.reddit.com/9/test',
            punctuation=459,
            creation_date=datetime.strptime(
                "2018-01-01T18:45:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=18
        )
        submission10 = Submission(
            title="Título 10",
            submitter=sub_user3,
            external_url='https://www.stackoverflow.com',
            discussion_url='https://www.reddit.com/10/test',
            punctuation=1280,
            creation_date=datetime.strptime(
                "2018-01-01T06:42:54+0000",
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            num_comments=2124
        )

        submission1.save()
        submission2.save()
        submission3.save()
        submission4.save()
        submission5.save()
        submission6.save()
        submission7.save()
        submission8.save()
        submission9.save()
        submission10.save()

    def test_response_popular(self):

        response = self.client.get(
            '/submissions/popular?{}'.format(self.external_type_param)
        )

        self.assertEqual(
            [
                {
                    'id': 10,
                    'submitter_name': 'user3',
                    'title': 'Título 10',
                    'punctuation': 1280,
                    'num_comments': 2124,
                },
                {
                    'id': 4,
                    'submitter_name': 'user4',
                    'title': 'Título 4',
                    'punctuation': 789,
                    'num_comments': 5,
                },
                {
                    'id': 3,
                    'submitter_name': 'user3',
                    'title': 'Título 3',
                    'punctuation': 650,
                    'num_comments': 1000,
                },
                {
                    'id': 7,
                    'submitter_name': 'user1',
                    'title': 'Título 7',
                    'punctuation': 2,
                    'num_comments': 7,
                },
            ],
            response.data
        )

        response = self.client.get(
            '/submissions/popular?{}'.format(self.discussion_type_param)
        )
        self.assertEqual(
            [
                {
                    'id': 8,
                    'submitter_name': 'user2',
                    'title': 'Título 8',
                    'punctuation': 524,
                    'num_comments': 170,
                },
                {
                    'id': 9,
                    'submitter_name': 'user2',
                    'title': 'Título 9',
                    'punctuation': 459,
                    'num_comments': 18,
                },
                {
                    'id': 1,
                    'submitter_name': 'user1',
                    'title': 'Título 1',
                    'punctuation': 340,
                    'num_comments': 200,
                },
                {
                    'id': 6,
                    'submitter_name': 'user1',
                    'title': 'Título 6',
                    'punctuation': 99,
                    'num_comments': 180,
                },
                {
                    'id': 2,
                    'submitter_name': 'user2',
                    'title': 'Título 2',
                    'punctuation': 67,
                    'num_comments': 500,
                },
                {
                    'id': 5,
                    'submitter_name': 'user1',
                    'title': 'Título 5',
                    'punctuation': 23,
                    'num_comments': 128,
                },
            ],
            response.data
        )

        response = self.client.get(reverse('popular'))
        self.assertEqual(
            [
                {
                    'id': 10,
                    'submitter_name': 'user3',
                    'title': 'Título 10',
                    'punctuation': 1280,
                    'num_comments': 2124,
                },
                {
                    'id': 4,
                    'submitter_name': 'user4',
                    'title': 'Título 4',
                    'punctuation': 789,
                    'num_comments': 5,
                },
                {
                    'id': 3,
                    'submitter_name': 'user3',
                    'title': 'Título 3',
                    'punctuation': 650,
                    'num_comments': 1000,
                },
                {
                    'id': 8,
                    'submitter_name': 'user2',
                    'title': 'Título 8',
                    'punctuation': 524,
                    'num_comments': 170,
                },
                {
                    'id': 9,
                    'submitter_name': 'user2',
                    'title': 'Título 9',
                    'punctuation': 459,
                    'num_comments': 18,
                },
                {
                    'id': 1,
                    'submitter_name': 'user1',
                    'title': 'Título 1',
                    'punctuation': 340,
                    'num_comments': 200,
                },
                {
                    'id': 6,
                    'submitter_name': 'user1',
                    'title': 'Título 6',
                    'punctuation': 99,
                    'num_comments': 180,
                },
                {
                    'id': 2,
                    'submitter_name': 'user2',
                    'title': 'Título 2',
                    'punctuation': 67,
                    'num_comments': 500,
                },
                {
                    'id': 5,
                    'submitter_name': 'user1',
                    'title': 'Título 5',
                    'punctuation': 23,
                    'num_comments': 128,
                },
                {
                    'id': 7,
                    'submitter_name': 'user1',
                    'title': 'Título 7',
                    'punctuation': 2,
                    'num_comments': 7,
                },
            ],
            response.data
        )

    def test_response_discussions(self):

        response = self.client.get(
            '/submissions/discussions?{}'.format(self.external_type_param)
        )
        self.assertEqual(
            [
                {
                    'id': 10,
                    'submitter_name': 'user3',
                    'title': 'Título 10',
                    'punctuation': 1280,
                    'num_comments': 2124,
                },
                {
                    'id': 3,
                    'submitter_name': 'user3',
                    'title': 'Título 3',
                    'punctuation': 650,
                    'num_comments': 1000,
                },
                {
                    'id': 7,
                    'submitter_name': 'user1',
                    'title': 'Título 7',
                    'punctuation': 2,
                    'num_comments': 7,
                },
                {
                    'id': 4,
                    'submitter_name': 'user4',
                    'title': 'Título 4',
                    'punctuation': 789,
                    'num_comments': 5,
                },
            ],
            response.data
        )

        response = self.client.get(
            '/submissions/discussions?{}'.format(self.discussion_type_param)
        )
        self.assertEqual(
            [
                {
                    'id': 2,
                    'submitter_name': 'user2',
                    'title': 'Título 2',
                    'punctuation': 67,
                    'num_comments': 500,
                },
                {
                    'id': 1,
                    'submitter_name': 'user1',
                    'title': 'Título 1',
                    'punctuation': 340,
                    'num_comments': 200,
                },
                {
                    'id': 6,
                    'submitter_name': 'user1',
                    'title': 'Título 6',
                    'punctuation': 99,
                    'num_comments': 180,
                },
                {
                    'id': 8,
                    'submitter_name': 'user2',
                    'title': 'Título 8',
                    'punctuation': 524,
                    'num_comments': 170,
                },
                {
                    'id': 5,
                    'submitter_name': 'user1',
                    'title': 'Título 5',
                    'punctuation': 23,
                    'num_comments': 128,
                },
                {
                    'id': 9,
                    'submitter_name': 'user2',
                    'title': 'Título 9',
                    'punctuation': 459,
                    'num_comments': 18,
                },
            ],
            response.data
        )

        response = self.client.get(reverse('discussions'))
        self.assertEqual(
            [
                {
                    'id': 10,
                    'submitter_name': 'user3',
                    'title': 'Título 10',
                    'punctuation': 1280,
                    'num_comments': 2124,
                },
                {
                    'id': 3,
                    'submitter_name': 'user3',
                    'title': 'Título 3',
                    'punctuation': 650,
                    'num_comments': 1000,
                },
                {
                    'id': 2,
                    'submitter_name': 'user2',
                    'title': 'Título 2',
                    'punctuation': 67,
                    'num_comments': 500,
                },
                {
                    'id': 1,
                    'submitter_name': 'user1',
                    'title': 'Título 1',
                    'punctuation': 340,
                    'num_comments': 200,
                },
                {
                    'id': 6,
                    'submitter_name': 'user1',
                    'title': 'Título 6',
                    'punctuation': 99,
                    'num_comments': 180,
                },
                {
                    'id': 8,
                    'submitter_name': 'user2',
                    'title': 'Título 8',
                    'punctuation': 524,
                    'num_comments': 170,
                },
                {
                    'id': 5,
                    'submitter_name': 'user1',
                    'title': 'Título 5',
                    'punctuation': 23,
                    'num_comments': 128,
                },
                {
                    'id': 9,
                    'submitter_name': 'user2',
                    'title': 'Título 9',
                    'punctuation': 459,
                    'num_comments': 18,
                },
                {
                    'id': 7,
                    'submitter_name': 'user1',
                    'title': 'Título 7',
                    'punctuation': 2,
                    'num_comments': 7,
                },
                {
                    'id': 4,
                    'submitter_name': 'user4',
                    'title': 'Título 4',
                    'punctuation': 789,
                    'num_comments': 5,
                },
            ],
            response.data
        )

    def test_response_top_submitters(self):

        response = self.client.get(reverse('submitters'))
        self.assertEqual(
            [
                {
                    'id': 1,
                    'user_name': 'user1',
                    'total_submissions': 4,
                },
                {
                    'id': 2,
                    'user_name': 'user2',
                    'total_submissions': 3,
                },
                {
                    'id': 3,
                    'user_name': 'user3',
                    'total_submissions': 2,
                },
                {
                    'id': 4,
                    'user_name': 'user4',
                    'total_submissions': 1,
                },
            ],
            response.data
        )

    def test_response_user_posts(self):

        response = self.client.get(reverse('user_posts', kwargs={'user_id': 1}))
        self.assertEqual(
            [
                {
                    'id': 7,
                    'submitter_name': 'user1',
                    'title': 'Título 7',
                    'punctuation': 2,
                    'num_comments': 7,
                },
                {
                    'id': 6,
                    'submitter_name': 'user1',
                    'title': 'Título 6',
                    'punctuation': 99,
                    'num_comments': 180,
                },
                {
                    'id': 5,
                    'submitter_name': 'user1',
                    'title': 'Título 5',
                    'punctuation': 23,
                    'num_comments': 128,
                },
                {
                    'id': 1,
                    'submitter_name': 'user1',
                    'title': 'Título 1',
                    'punctuation': 340,
                    'num_comments': 200,
                },
            ],
            response.data
        )
