
from django.db.models import Count
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from blueliv_rest.models import Submission, SubredditUser
from blueliv_rest.serializers import (SubmissionSerializer,
                                      SubredditUserSerializer)


class MostPopular(APIView):
    """ Class mapping url resource for getting top 10 submissions by points """

    def get(self, request):
        top_submitted = None
        type_param = request.GET.get('type', None)
        if type_param:
            if type_param == 'discussion':
                top_submitted = Submission.objects.filter(
                    external_url__isnull=True
                ).order_by('-punctuation')[:10]
            elif type_param == 'external':
                top_submitted = Submission.objects.filter(
                    external_url__isnull=False
                ).order_by('-punctuation')[:10]
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            top_submitted = (
                Submission.objects.all().order_by('-punctuation')[:10]
            )

        serializer = SubmissionSerializer(top_submitted, many=True)
        return Response(serializer.data)


class MostDiscussed(APIView):
    """ Class mapping url resource for getting top 10 discussed submissions """

    def get(self, request):
        top_discussed = None
        type_param = request.GET.get('type', None)
        if type_param:
            if type_param == 'discussion':
                top_discussed = Submission.objects.filter(
                    external_url__isnull=True
                ).order_by('-num_comments')[:10]
            elif type_param == 'external':
                top_discussed = Submission.objects.filter(
                    external_url__isnull=False
                ).order_by('-num_comments')[:10]
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            top_discussed = (
                Submission.objects.all().order_by('-num_comments')[:10]
            )

        serializer = SubmissionSerializer(top_discussed, many=True)
    return Response(serializer.data)


class TopSubmitters(APIView):
    """ Class mapping url resource for getting top submitters """

    def get(self, request):

        top_submitters = (
            SubredditUser.objects.annotate(
                total_submissions=Count('submission')
            ).order_by('-total_submissions')[: 10]
        )

        serializer = SubredditUserSerializer(top_submitters, many=True)
        return Response(serializer.data)


class UserSubmissions(APIView):
    """ Class mapping url resource for getting posts by user """

    def get(self, request, user_id):
        user_posts = None
        if user_id:
            try:
                sub_user = SubredditUser.objects.get(pk=user_id)
                user_posts = Submission.objects.filter(
                    submitter=sub_user
                ).order_by('-creation_date')
            except SubredditUser.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = SubmissionSerializer(user_posts, many=True)
        return Response(serializer.data)
