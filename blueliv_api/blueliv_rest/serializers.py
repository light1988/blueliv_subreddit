
from rest_framework import serializers

from blueliv_rest.models import Submission, SubredditUser


class SubmissionSerializer(serializers.ModelSerializer):
    """ Class for serializing Submission model """

    submitter_name = serializers.CharField(
        source='submitter.user_name', read_only=True
    )

    class Meta:
        model = Submission
        fields = (
            'id', 'submitter_name', 'title', 'punctuation', 'num_comments'
        )


class SubredditUserSerializer(serializers.ModelSerializer):
    """ Class for serializing SubredditUser model """

    total_submissions = serializers.IntegerField()

    class Meta:
        model = SubredditUser
        fields = (
            'id', 'user_name', 'total_submissions'
        )
